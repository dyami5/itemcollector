//
//  ViewController.swift
//  ItemCollector
//
//  Created by Jose Ma. P. Alvarado on 21/03/2017.
//  Copyright © 2017 1010. All rights reserved.
//

import UIKit

class ViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {

    
    @IBOutlet weak var tableView: UITableView!
    
    
    // Standard code to get stuff from core data
    var items : [Item] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        tableView.dataSource = self
        tableView.delegate = self
        
    }

    // Standard function that is called each time this viewcontroller appears
    override func viewWillAppear(_ animated: Bool) {
        
        // Standard code to use existing functions in Appdelegate to access core data
        let context = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
        
        // Standard code to fetch data from core data
        do {
            items = try context.fetch(Item.fetchRequest())
            tableView.reloadData()
        } catch {
            
        }
        
    }
    
    // Required standard function for tableview
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return items.count
    }
    
    // Required standard function for tableview
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = UITableViewCell()
        let item = items[indexPath.row]
        cell.textLabel?.text = item.title
        cell.imageView?.image = UIImage(data: item.image as! Data)
        return cell
    }
    
    // Standard function for passing data and moving to a new viewController upon tapping an item in tableview
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let item = items[indexPath.row]
        
        performSegue(withIdentifier: "itemSegue", sender: item)
    
    }
    
    // Standard code to pass on value from tableview to another viewController
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        let nextVC = segue.destination as! ItemViewController
        nextVC.item = sender as? Item
        
    }
    
    
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

