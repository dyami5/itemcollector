//
//  ItemViewController.swift
//  ItemCollector
//
//  Created by Jose Ma. P. Alvarado on 21/03/2017.
//  Copyright © 2017 1010. All rights reserved.
//

import UIKit

class ItemViewController: UIViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate {

    @IBOutlet weak var itemImageView: UIImageView!
    
    @IBOutlet weak var titleTextField: UITextField!
    
    @IBOutlet weak var addupdateButton: UIButton!
    
    @IBOutlet weak var deleteButton: UIButton!
    

    // Standard code needed to access photos/camera in the phone: requires setup of delegates in the class "UIImagePickerControllerDelegate, UINavigationControllerDelegate" and another property in info.plist "Privacy - Photo Library Usage"
    var imagePicker = UIImagePickerController()
    
    // Code to be used when an app feature uses the same viewController
    var item : Item? = nil
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        // Standard code needed for imagePickerController to work
        imagePicker.delegate = self
        
        // IF statement is used to control objects in accordance to given parameters
        if item != nil {
            
            itemImageView.image = UIImage(data: item!.image as! Data)
            titleTextField.text = item!.title
            
            addupdateButton.setTitle("Update", for: .normal)
        } else {
            
            deleteButton.isHidden = true
            
        }
        
    }

    
    @IBAction func photosTapped(_ sender: Any) {
        
        // Standard code to access the photo library
        imagePicker.sourceType = .photoLibrary
        
        // Standard code to present the photo library of the phone
        present(imagePicker, animated: true, completion: nil)
    }
    
    // Standard function to allow chosen image from photo library to put it in the app
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        
        let image = info[UIImagePickerControllerOriginalImage] as! UIImage
        
        itemImageView.image = image
        
        // Standard code to dismiss the photo library screen. This is required everytime you call the photo library.
        imagePicker.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func cameraTapped(_ sender: Any) {
        
        // Standard code to access the photo library
        imagePicker.sourceType = .camera
        
        // Standard code to present the photo library of the phone
        present(imagePicker, animated: true, completion: nil)

        
    }
    
    @IBAction func addTapped(_ sender: Any) {
        
        if item != nil {
            
            item!.title = titleTextField.text
            item!.image = UIImagePNGRepresentation(itemImageView.image!) as NSData?
            
        } else {
            
            // Standard code to use existing functions in Appdelegate to access core data
            let context = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
            
            // Standard code to add data on the item entity in core data
            let item = Item(context: context)
            
            item.title = titleTextField.text
            item.image = UIImagePNGRepresentation(itemImageView.image!) as NSData?
            
        }
        
        
        // Standard code to save changes to core data
        (UIApplication.shared.delegate as! AppDelegate).saveContext()
        
        // Standard code to automatically return to previous viewController
        navigationController!.popViewController(animated: true)
    }
    
    
    @IBAction func deleteTapped(_ sender: Any) {
        
        let context = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
        
        // Standard code to delete data on item entity in core data
        context.delete(item!)
        
        // Standard code to save changes to core data
        (UIApplication.shared.delegate as! AppDelegate).saveContext()
        
        // Standard code to automatically return to previous viewController
        navigationController!.popViewController(animated: true)
    }
    
    
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

  }
